#!/usr/bin/python3

import sys, importlib

def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)

def enforce_default(arg, default):
    if not arg:
        return default
    return arg

enf_dlt = enforce_default

def import_(module, exc_dict={'ImportError':'"{lib_name}" possibly not found.'}, code=1):
    try:
        module_ = importlib.import_module(module)
    except Exception as err:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        cls = str(type(err)).replace('<class \'', '').replace('\'>', '') # class
        msg = str(err) # message
        trace_back = traceback.extract_tb(exc_traceback)
        stack_trace = ''
        for i in range(len(trace_back)):
            trace = trace_back[i]
            n = len(trace_back) - i
            if 2 - len(str(n)) > 0:
                n = str(n) + (4 - len(str(n))) * ' '
            stack_trace += '''{}:{}\n{}{}\n    {}\n'''.format(trace[0], trace[1], n, trace[2], trace[3])
        print(exc_dict[cls].format(lib_name=module, cls=cls, exc_msg=msg))
        print('{}: {}'.format(cls, msg))
        print(stack_trace)
        exit_(code)
        return None
    else:
        print('Library "{}" imported successfully.'.format(module))
        return module_

if __name__ == '__main__':
    try:
        logulator = import_('logulator')
        if not logulator:
            exit_(10)
        # import logulator

        print(logulator.info.__showstr__)

        argparse = import_('argparse')
        if not logulator:
            exit_(11)
        # import argparse

        parser = argparse.ArgumentParser(prog='logulator')
        parser.add_argument('-ga',  '--geo-api',     '--geo-apis',       dest='ga',   help='''Default: "geopy"
                                                                                              Options: "googlemaps", "geopy"
                                                                                              List of APIs to use in get_len, in order from priority to non-priority. For calculating distance between stops''')
        parser.add_argument('-gak', '--geo-api-key', '--geo-api-keys',   dest='gak',  help='''Default: None
                                                                                              Options: None
                                                                                              List of API keys for APIs that use API Keys''')
        parser.add_argument('-ba',  '--bus-api',                         dest='ba',   help='''Default: "nextbus"
                                                                                              Options: "nextbus" API to use in log_*. For getting [near] real-time bus route & loc data''')
        parser.add_argument('-t',   '--test',        '--test-mode',      dest='test', help='''Default: False
                                                                                              Options: True, False
                                                                                              Enable test mode.''', action='store_true')
        parser.add_argument('-u', '--unicode',       '--unicode',         dest='uc',  help='''Default: False
                                                                                              Options: True, False
                                                                                              Enable unicode characters.''', action='store_true')
        parser.add_argument('-a',   '--agency',      '--nb-agency',      dest='a',    help='''Default: "ttc"
                                                                                              Options: <all agencies listed in bus-api>
                                                                                              Agency tag (eg San Francisco Muni is "sf-muni", Toronto TC (TTC) is "ttc")''')
        parser.add_argument('-slp', '--sl-path',     '--stoplist-path',  dest='slp',  help='''Default: "data_stop_1568411857_ttc_34.csv"
                                                                                              Options: re"data_stop_.*_.*_.*.csv"
                                                                                              Stoplist Path.''')
        parser.add_argument('-rlp', '--rl-path',     '--routelist-path', dest='rlp',  help='''Default: "data_route_1568414604_ttc_34.csv"
                                                                                              Options: re"data_route_.*_.*_.*.csv"
                                                                                              Routelist Path.''')
        parser.add_argument('-rtt', '--rt-tag',      '--route-tag',      dest='rtt',  help='''Default: "34_1_34Akes"
                                                                                              Options: re".*_.*_.*"
                                                                                              Route Tag.''')
        parser.add_argument('-rid', '--r-id',        '--route-id',       dest='rid',  help='''Default: rtt.split('_')[0]
                                                                                              Options: re".*"
                                                                                              Route ID.''')
        parser.add_argument('-itv', '--intv',        '--interval',       dest='itv',  help='''Default: 1000
                                                                                              Options: re".*"
                                                                                              Heartbeat Interval.''')
        parser.add_argument('-dp',  '--datap',       '--data-path',      dest='dp',   help='''Default: .
                                                                                              Options: re".*"
                                                                                              Directory to find data files (data_.*.csv).''')
        parser.add_argument('mode',                                                   help='''Default: None
                                                                                              Options: "get-len-sl", "get-len-rt", "get-len-rc", "log-bus", "log-route"
                                                                                              Mode for Logulator. If None, exits.''')
        args = parser.parse_args()
        args.ga  = enf_dlt(args.ga,  'geopy')
        args.ba  = enf_dlt(args.ba,  'nextbus')
        args.a   = enf_dlt(args.a,   'ttc')
        args.slp = enf_dlt(args.slp, 'data_stop_1568411857_ttc_34.csv')
        args.rlp = enf_dlt(args.rlp, 'data_route_1568414604_ttc_34.csv')
        args.rtt = enf_dlt(args.rtt, '34_1_34Akes')
        args.rid = enf_dlt(args.rid, args.rtt.split('_')[0])
        args.itv = enf_dlt(args.itv, 1000)
        args.dp  = enf_dlt(args.dp, '.')
        modes_desc = {'get-len-sl':'Uses get_len module to create a data_chart from data_route and data_stop. data_route and data_stop has to be of the same agency and route (branches and directions do not matter).',
                      'get-len-rt':'Uses get_len module to find the closest stop to a real-life bus from bus-loc-api.',
                      'get-len-rc':'Uses get_len module to create a real chart',
                      'log-bus'   :'Logs rt bus loc + extra info.',
                      'log-route' :'Loga route info + extra info.',
                      'hb-rc'     :'Initate Heartbeat for get-rc.',
                      'hb-sys'    :'System Heartbeat.',
                      'hb-test'   :'Test Heartbeat.',
                      'filter'    :'Filter CSVs for target SID.', }
        if args.mode in modes_desc.keys():
            print('Mode:')
            print('  ID:    {}'.format(args.mode))
            print('  Notes: {}'.format(modes_desc[args.mode]))
        else:
            print('The mode you specified, "{}" isn\'t supported.'.format(args.mode))
            exit_(4)
        print('Testing: {}'.format(args.test))
        print()

        if args.mode == 'get-len':
            if args.test:
                logulator.get_len.qs_test(args.slp, args.rlp, args.rtt, args,rid)
            else:
                logulator.get_len.qs(args.slp, args.rlp, args.rtt, args.rid)

        elif args.mode == 'get-len-sl':
            if args.test:
                logulator.get_len.qs_sl_test(args.slp, args.rlp, args.rtt)
            else:
                glogulator.et_len.qs_sl(args.slp, args.rlp, args.rtt)

        elif args.mode == 'get-len-rc':
            if args.test:
                logulator.get_len.qs_rc_test(args.slp, args.rtt, args.rid, args.a, ga=args.ga, ba=args.ba)
            else:
                logulator.get_len.qs_rc(args.slp, args.rtt, args.rid, args.a, ga=args.ga, ba=args.ba)

        elif args.mode == 'get-len-rt':
            if args.test:
                logulator.get_len.qs_rt_test(args.slp, args.a, args.rid)
            else:
                logulator.get_len.qs_rt(args.slp, args.a, args.rid)

        elif args.mode == 'log-bus':
            if args.test:
                logulator.log_bus.qs_test()
            else:
                logulator.log_bus.qs()

        elif args.mode == 'log-route':
            if args.test:
                logulator.log_route.qs_test()
            else:
                logulator.log_route.qs()

        elif args.mode == 'hb-rc':
            if args.test:
                logulator.heartbeat.rc_heartbeat(args.slp, args.rtt, args.rid, args.a, ga=args.ga, ba=args.ba, interval=args.itv, test=True,  unicode=args.uc)
            else:
                logulator.heartbeat.rc_heartbeat(args.slp, args.rtt, args.rid, args.a, ga=args.ga, ba=args.ba, interval=args.itv, test=False, unicode=args.uc)

        elif args.mode == 'hb-sys':
            logulator.heartbeat.sys_heartbeat(args.ga, args.gak, args.ba, args.test, args.uc, args.a, args.rtt, args.rid, args.itv, args.dp)

        elif args.mode == 'hb-test':
            print('Heartbeat Test max_=1')
            logulator.heartbeat.sys_heartbeat(args.ga, args.gak, args.ba, args.test, args.uc, args.a, args.rtt, args.rid, args.itv, args.dp, max_=1)
        
        elif args.mode == 'hb-sys-day':
            print('Heartbeat System for 24 hours')
            logulator.heartbeat.sys_heartbeat(args.ga, args.gak, args.ba, args.test, args.uc, args.a, args.rtt, args.rid, args.itv, args.dp, max_h=24)
        
        elif args.mode == 'filter':
            print('Filter')
            logulator.filter_.filter_(test=args.test)

        else:
            print('There is a possible bug in the program. Please report it.')


    except KeyboardInterrupt:
        print('\n\nInterrupted. (Possibly Ctrl-C) (KeyboardInterrupt)')
        exit_(9)
