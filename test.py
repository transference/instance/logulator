#!/usr/bin/python3

import sys, importlib

def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)

def enforce_default(arg, default):
    if not arg:
        return default
    return arg

enf_dlt = enforce_default

def import_(module, exc_dict={'ImportError':'"{lib_name}" possibly not found.'}, code=1):
    try:
        module_ = importlib.import_module(module)
    except Exception as err:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        cls = str(type(err)).replace('<class \'', '').replace('\'>', '') # class
        msg = str(err) # message
        trace_back = traceback.extract_tb(exc_traceback)
        stack_trace = ''
        for i in range(len(trace_back)):
            trace = trace_back[i]
            n = len(trace_back) - i
            if 2 - len(str(n)) > 0:
                n = str(n) + (4 - len(str(n))) * ' '
            stack_trace += '''{}:{}\n{}{}\n    {}\n'''.format(trace[0], trace[1], n, trace[2], trace[3])
        print(exc_dict[cls].format(lib_name=module, cls=cls, exc_msg=msg))
        print('{}: {}'.format(cls, msg))
        print(stack_trace)
        exit_(code)
        return None
    else:
        print('Library "{}" imported successfully.'.format(module))
        return module_

logulator = import_('logulator')
if logulator == None:
    exit_(1)
logulator.heartbeat
