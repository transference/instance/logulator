# Transference Instance Logulator

Collects data from Bus APIs (only NextBus as of nov-27-2019)

## Structure

```
Bus API   Geo API
   \         /   
    Logulator    
        |
     Learner
        |
     Hamster
```

## Installing

Installing requires `sudo` privelledges.

### 1. Setup Environ

```bash
sudo adduser transference --home /home
sudo mkdir -p /home/transference/instance/logulator
cd /home/transference/instance/logulator
```

### 2. Clone

```bash
sudo git clone https://gitlab.com/transference/instance/logulator.git
```

### 3. Setup Cron

```bash
crontab -e
```

1. Select an editor (if asked)
2. In a new line, type:
```cron
0 0 * * 0 bash /home/transference/instance/logulator/run.sh
```

## Installing (legacy)

* Requires `sudo` permissions from user ran
* Using wget and deleting since curl sometimes is not installed / does not work (corrupts text)

```bash
curl "https://gitlab.com/transference/instance/logulator/raw/master/logulator/install.sh" > install_logulator.sh | bash install_logulator.sh && rm install_logulator.sh
```

## Error Codes

### 422537

Error occurred outside heartbeat, but inside heartbeat process.

### 422538

Error occurred inside heartbeat.
