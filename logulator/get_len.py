import csv
import geopy.distance as geopy_dist
# geopy_dist = None
import googlemaps
import json
import time
import warnings
import requests
from datetime import datetime
from . import *
from . import nextbus
from . import time_util

def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)


def _get_len_(coords_0, coords_1, api='googlemaps'):
    if api == 'googlemaps':
        coords_0 = '{lat}, {lon}'.format(lon=coords_0[0], lat=coords_0[1])
        coords_1 = '{lat}, {lon}'.format(lon=coords_1[0], lat=coords_1[1])
        # GC Maps Directions API
        # coords_0 = '43.70721,-79.3955999'
        # coords_1 = '43.7077599,-79.39294'
        gmaps = googlemaps.Client(key="AIzaSyBAFZj11dNs_6od98sAFJnSppVaA4VwJgk")
        # Request directions via public transit
        now = datetime.now()
        directions_result = gmaps.directions(coords_0, coords_1, mode="driving", departure_time=now, avoid='tolls')

        distance = 0
        legs = directions_result[0].get("legs")
        for leg in legs:
            distance = distance + leg.get("distance").get("value")
        return distance
    elif api == 'geopy':
        return geopy_dist.distance(coords_0, coords_1).km

def _get_len(points, additive=False, allow_skip=False, skip=False, header=[['length', 'tag_0', 'tag_1']], api='geopy',
             api_key='AIzaSyDnjQoabH_SZ8B7oSES_lttQKbnZ5T9LFo'):
    '''
    Gets length of between `points`. DO NOT USE `googlemaps` API!
    APIs:
    `googlemaps`: More accurate, online/API key required
    `geopy`: Less accurate, offline/free
    :param points: List of points. Look at `_get_mapped` source for format
    :type points: list
    :param additive: If length is additive. Only `True` for buses, not stops.
    :type additive: bool
    :param allow_skip: Turn on skipping
    :type allow_skip: bool
    :param skip: When to skip. `True`= skip,non-skip,skip,... `False`=non-skip,skip,non-skip,...
    :type skip: bool
    :param header: CSV header. (must be 2d list with header in 1st list. eg [['length', 'tag_0', 'tag_1']]
    :type header: list
    :param api: API to use. `googlemaps` = Google Cloud Maps Directions API `geopy` = GeoPy.distance.distance (vincenty as of Sep 2019)
    :type api: str
    :return: raw data before CSV formattingz
    :raises: None, except bug in code
    '''
    try:
        requests.get('https://googleapis.com')
    except requests.ConnectionError:
        warnings.warn('Google APIs is unavailable, using GeoPy.', RuntimeWarning) # TODO: Fix this, hard-coded.
        api = 'geopy'
    api = api.lower()
    points = points[1:]
    route = header
    del header
    for i in range(len(points)):
        skip = not skip
        if skip:
            if allow_skip:
                continue
        # GeoPy
        if i + 1 == len(points):
            tag_0 = points[i][2]
            tag_1 = points[i - 1][2]
            coords_0 = [points[i][1], points[i][0]]
            coords_1 = [points[i - 1][1], points[i - 1][0]]
        else:
            coords_0 = [points[i][1], points[i][0]]
            coords_1 = [points[i + 1][1], points[i + 1][0]]
            tag_0 = points[i][2]
            tag_1 = points[i + 1][2]
        distance = _get_len_(coords_0, coords_1, api)
        if additive:
            if i == 0:
                route.append([distance, tag_0, tag_1])
            else:
                route.append([float(route[-1][0]) + distance, tag_0, tag_1])
        else:
            route.append([distance, tag_0, tag_1])
    return route

def _get_mapped(stoplist, order, tag):
    '''
    Get `points` from `stoplist` and `order`.
    :param stoplist: Stoplist data. Headers ommited.
    :type stoplist: list
    :param order:    Order data. Headers ommited.
    :type order: list
    :param tag:      Tag. eg `34_1_34Akes`
    :type tag: str
    :return: `ordered_list` (list in order from tag)
    :raises: None
    '''
    ordered_list = []
    order_match = []
    for i in range(len(order)):
        order_entry = order[i]
        if order_entry[0] == tag:
            order_match.append(i)
    del tag
    if order_match == []:
        warnings.warn('None matched to tag. Returning False.', RuntimeWarning)
        return False
    if len(order_match) > 1:
        warnings.warn('More than one `order_match`. Using 1st.', RuntimeWarning)
    order_match = order_match[0]
    stoplist_filter = json.loads(order[order_match][2].replace('\'', '"'))  # Hotfix for some CSVs.
    for i in range(len(stoplist_filter)):
        stoplist_filter_entry = stoplist_filter[i]
        for j in range(len(stoplist)):  # TODO: Inefficient, fix later. #3
            stoplist_entry = stoplist[j]
            if stoplist_entry[3] in stoplist_filter_entry:
                ordered_list.append([float(stoplist_entry[0]), float(stoplist_entry[1]), stoplist_entry[3]])
    return ordered_list

def get_len(stoplist_path, order_path, tag, additive=False, allow_skip=False, skip=False,
            header=[['length', 'tag_0', 'tag_1']], api='geopy', api_key='AIzaSyDnjQoabH_SZ8B7oSES_lttQKbnZ5T9LFo', test=False):
    '''
    Gets length of stops for `tag`.
    :param stoplist_path: Path to stoplist csv (data_stop_.*_.*_.*.csv).
    :type stoplist_path: str
    :param order_path: Path to order/route csv (data_route_.*_.*_.*.csv).
    :type order_path: str
    :param tag: Route tag (.*_.*_.*) eg 34_1_34Akes.
    :type tag: str
    :param additive: If length is additive. Only `True` for buses, not stops.
    :type additive: bool
    :param allow_skip: Turn on skipping
    :type allow_skip: bool
    :param skip: When to skip. `True`= skip,non-skip,skip,... `False`=non-skip,skip,non-skip,...
    :type skip: bool
    :param header: CSV header. (must be 2d list with header in 1st list. eg [['length', 'tag_0', 'tag_1']]
    :type header: list
    :param api: API to use for length. `googlemaps`: (online, 0.04~0.05 cents/request (approximately), accurate-er than geopy) Google Cloud Maps Directions API `geopy`: (offline, free, inaccurate) geopy.distance.distance (vincenty as of Sep 2019)
    :type api: str
    :return: Path of saved csv.
    '''
    with open(stoplist_path, 'r') as csv_file:
        stoplist = list(csv.reader(csv_file))
    with open(order_path, 'r') as csv_file:
        order = list(csv.reader(csv_file))
    del order_path, csv_file
    points = _get_mapped(stoplist[1:], order[1:], tag)
    route = _get_len(points, additive=additive, allow_skip=allow_skip, skip=skip, header=header, api=api,
                     api_key=api_key)
    if test:
        save_path = 'data_chart_test_{}_{}_{}.csv'.format(str(int(time_util.epoch_ms()())), stoplist_path.split('_')[3],
                                                          stoplist_path.split('_')[4])
    else:
        save_path = 'data_chart_{}_{}_{}.csv'.format(str(int(time_util.epoch_ms()())), stoplist_path.split('_')[3],
                                                     stoplist_path.split('_')[4])
    with open(save_path, 'w') as csv_file:
        csv_w = csv.writer(csv_file, delimiter=',')
        csv_w.writerows(route)
    return save_path

def _map_bus_to_stop(stops, bus_loc, api='geopy', api_key='AIzaSyBAFZj11dNs_6od98sAFJnSppVaA4VwJgk'):
    '''
    Maps bus loc to stop.
    :param stops: List of stops. lon,lat,id,tag[,title]
    :type stops: list
    :param bus_loc: Location of bus in [lon, lat]
    :type bus_loc: list
    :param api: API to use. `googlemaps` = Google Cloud Maps Directions API `geopy` = GeoPy.distance.distance (vincenty as of Sep 2019)
    :type api: str
    :param api_key: API Key, if using an APi that requires an API key.
    :type api_key: str
    :return: NextBus Stop Tag
    '''
    distance = None
    tag = None
    stops = stops[1:]
    for i in range(len(stops)):  # TODO: Inefficient, fix later.
        stop = stops[i]
        tag_0 = stops[i][3]
        coords_0 = [float(stops[i][1]), float(stops[i][0])]
        coords_1 = [float(bus_loc[1]), float(bus_loc[0])]
        cand_distance = _get_len_(coords_0, coords_1, api)
        if not distance:
            distance = cand_distance
            tag = tag_0
        elif distance > cand_distance:
            distance = cand_distance
            tag = tag_0
    return tag

def _get_sid(bus, a, id, time, cont = None):
    '''
    Get bus SID.
    :param bus: Dict from NextBus JSON extract.
    :type bus: dict
    :param a: Agency Tag
    :type a: str
    :return: SID
    '''
    if cont == None:
        file = open('sid_count.txt', 'r')
        cont = file.read()
        file.close()
        dt = datetime.now()
    new_cont = ''
    for line in cont.split('\n'):
        if '{y}'.format(y = dt.year) in line:
            if '{y} {d} {a} {dir}'.format(y = dt.year, d = dt.day, a = a, dir = dir) in line: # easier to understand: y d a dir count
                count = line.split(' ')[4]
                new_cont += '{y} {d} {a} {dir} {count}'.format(y = dt.year, d = dt.day, a = a, dir = dir, count = count) + '\n'
            else:
                new_cont += line + '\n'
    nb = nextbus.qs()
    if 'dirTag' in bus.keys():
        dir = bus['dirTag']
        r = dir.split('_')[0]
        nb.get_route_schedule(a, r)
    else:
        dir = 'no_dir_tag'
    if cont != new_cont:
        file = open('sid_count.txt', 'w')
        file.write(new_cont)
        file.close()
    return ['{}_{}_{}'.format(a, dir, count + 1), new_cont]

def _get_real_chart(sl, rtt, buses, a, ga='googlemaps'):
    time_now = time_util.day_ms()
    try:
        import datetime
    except ImportError as error:
        msg = str(error)
        print(
            'Python Standard Library "datetime" possibly not found, or there is an ImportError inside the module.(ImportError: {})\nYour Instance instance might be installed incorrectly or corrupted. Try reinstalling, and if the problem persists or you think that this is a bug, please report it.'.format(
                msg))
        exit_(8)
    else:
        print('Library "datetime" imported successfully.')
    print('Gen "start", "data"...')
    start = sl[0]
    data = [['sid', 'dist', 'millitime']]
    print('Done.')
    print('Looping...')
    cont = None
    for i in range(len(buses)):
        print('Loop {}/{}...'.format(i, len(buses) - 1), end=' ')
        bus = buses[i]
        sid, cont = _get_sid(bus, a, bus['id'], time_now, cont)
        dist = _get_len_((bus['lon'], bus['lat']), (start[0], start[1]), ga)
        data.append([sid, dist, time_now - int(bus['secsSinceReport'])*1000])
        print('Done.')
    print('Done.')
    return data


def qs_rc(slp, rtt, rid, a, ga='googlemaps', ba='nextbus', uuid='no_uuid'):
    print('Get Real Chart WIP')
    print('Stoplist Path:  {}'.format(slp))
    print('Route Tag:      {}'.format(rtt))
    if slp == '':
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rtt == '':
        rtt = '34_1_34Akes'
    with open(slp, 'r') as csv_file:
        sl = list(csv.reader(csv_file))[1:]
    nb = nextbus.qs()
    try:
        data = nb.get_route_loc(a, rid)
        if 'vehicle' not in data.keys():
            print('No vehicle key.')
            return 2
            # raise RuntimeError('No vehicle key.')
        else:
            buses = data['vehicle']
    except requests.exceptions.ConnectionError as error:
        print('Bus Loc API is unreacheable. (requests.exceptions.ConnectionError: {})'.format(str(error)))
        return 1
    data = _get_real_chart(sl, rtt, buses, a, ga)
    with open('data_rc_{}_{}_{}_{}.csv'.format(time_util.epoch_ms(), a, rtt, uuid), 'w') as file:
        csv_file = csv.writer(file)
        csv_file.writerows(data)

def qs_rc_test(slp, rtt, rid, a, ga='googlemaps', ba='nextbus', uuid='no_uuid'):
    print('Get Real Chart Test WIP')
    if slp == '':
        warnings.warn('slp not specified or ""; replaced with "data_stop_1568411857_ttc_34.csv".')
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rtt == '':
        warnings.warn('rtt not specified or ""; replaced with "34_1_34Akes"')
        rtt = '34_1_34Akes'
    print('Stoplist Path:  {}'.format(slp))
    print('Route Tag:      {}'.format(rtt))
    with open(slp, 'r') as csv_file:
        sl = list(csv.reader(csv_file))[1:]
    import requests, nextbus
    nb = nextbus.qs()
    try:
        buses = nb.get_route_loc(a, rid)['vehicle']
    except requests.exceptions.ConnectionError as error:
        print('Bus Loc API is unreacheable. (requests.exceptions.ConnectionError: {})'.format(str(error)))
        print('NextBus Webservices is unreachable. (requests.exceptions.ConnectionError)')
        print('The following conditions need to be met:')
        print('1. Connection to webservices.nextbus.com via HTTP(S)')
        print('Which usually need:')
        print('1. Connection to the Internet via HTTP(S)')
        print('2. No firewalls blocking access to webservices.nextbus.com or the Internet')
        print('3. No Wi-Fi/Local Network signin pages (eg Guest Wi-Fi signin pages)')
        print('4. TCP to be enabled on the local network, if it exists.')
        return
    data = _get_real_chart(sl, rtt, buses, a, ga)
    with open('data_rc_test_{}_{}_{}_{}.csv'.format(time_util.epoch_ms(), a, rtt, uuid), 'w') as file:
        csv_file = csv.writer(file)
        csv_file.writerows(data)
    print('Done.')

def qs_sl(slp, rlp, rtt):
    print('Get Stoplist')
    print('Stoplist Path:  {}'.format(slp))
    print('Routelist Path: {}'.format(rlp))
    print('Route Tag:      {}'.format(rtt))
    if slp == '':
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rlp == '':
        rlp = 'data_route_1568414604_ttc_34.csv'
    if rtt == '':
        rtt = '34_1_34Akes'
    save_path = get_len(slp, rlp, rtt)
    print()
    with open(slp, 'r') as csv_file:
        stops = list(csv.reader(csv_file))
    print('Done. Saved at' + save_path + '.')

def qs_sl_test(slp, rlp, rtt):
    print('Stoplist Test')
    print('Stoplist Path:  {}'.format(slp))
    print('Routelist Path: {}'.format(slp))
    print('Route Tag:      {}'.format(rtt))
    if slp == '':
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rlp == '':
        rlp = 'data_route_1568414604_ttc_34.csv'
    if rtt == '':
        rtt = '34_1_34Akes'
    save_path = get_len(slp, rlp, rtt, test=True)
    print()
    print('Done. Saved at' + save_path + '.')

def qs_rt(slp, a, rid):
    print('Get Real-time Bus Loc')
    print('Stoplist Path: {}'.format(slp))
    print('Route ID:      {}'.format(rid))
    svp = 'data_rt_{}_{}_{}.csv'.format(int(time_util.epoch_ms(), slp.split('_')[3], rid))
    with open(slp, 'r') as csv_file:
        stops = list(csv.reader(csv_file))
    nb = nextbus.qs()
    import requests
    try:
        buses = nb.get_route_loc(a, rid)['vehicle']
    except requests.exceptions.ConnectionError as error:
        print('Bus Loc API is unreacheable. (requests.exceptions.ConnectionError: {})'.format(str(error)))
    print('id,timestamp,tag')
    data = [['id', 'timestamp', 'tag']]
    for i in range(len(buses)):
        bus = buses[i]
        lon = buses[i]['lon']
        lat = buses[i]['lat']
        timestamp = int(time_util.epoch_s() - int(buses[i]['secsSinceReport']))
        data.append([bus['id'], timestamp, _map_bus_to_stop(stops, [float(lon), float(lat)])])
        print('{},{},{}'.format(*data[-1]))
    with open(svp, 'w') as csv_file:
        csv_w = csv.writer(csv_file)
        csv_w.writerows(data)
    print('Done. Saved at ' + svp + '.')

def qs_rt_test(slp, a, rid):
    print('Real-time Bus Loc Test')
    print('Stoplist Path: {}'.format(slp))
    print('Route ID:      {}'.format(rid))
    svp = 'data_rt_test_{}_{}_{}.csv'.format(int(time_util.epoch_ms(), slp.split('_')[3], rid))
    with open(slp, 'r') as csv_file:
        stops = list(csv.reader(csv_file))
    nb = nextbus.qs()
    import requests
    try:
        buses = nb.get_route_loc(a, rid)['vehicle']
    except requests.exceptions.ConnectionError:
        print('NextBus Webservices is unreachable. (requests.exceptions.ConnectionError)')
        print('The following conditions need to be met:')
        print('1. Connection to webservices.nextbus.com via HTTP(S)')
        print('Which usually need:')
        print('1. Connection to the Internet via HTTP(S)')
        print('2. No firewalls blocking access to webservices.nextbus.com or the Internet')
        print('3. No Wi-Fi/Local Network signin pages (eg Guest Wi-Fi signin pages)')
        print('4. TCP to be enabled on the local network, if it exists.')
    print('Finding closest for {} bus(es).'.format(len(buses)))
    print('id,timestamp,lon,lat,tag')
    data = [['id', 'timestamp', 'lon', 'lat', 'tag']]
    for i in range(len(buses)):
        bus = buses[i]
        lon = buses[i]['lon']
        lat = buses[i]['lat']
        timestamp = int(time_util.epoch_s() - int(buses[i]['secsSinceReport']))
        data.append([bus['id'], timestamp, lon, lat, _map_bus_to_stop(stops, [float(lon), float(lat)])])
        print('{},{},{},{},{}'.format(*data[-1]))
    with open(svp, 'w') as csv_file:
        csv_w = csv.writer(csv_file)
        csv_w.writerows(data)
    print('Done. Saved at ' + svp + '.')

def qs(slp, rlp, rtt, rid):
    qs_sl(slp, rlp, rtt)
    qs_rt(slp, rid)

def qs_test(slp, rlp, rtt, rid):
    qs_sl_test(slp, rlp, rtt)
    qs_rt_test(slp, rid)


if __name__ == '__main__':
    print('get_len.py')

