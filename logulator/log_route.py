import csv
import time
from . import nextbus
qs = nextbus.qs

def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)


nb = qs()


def log_route_stop(a, r, raw_data=None, time=time.time()):
    save_path = 'data_stop_{}_{}_{}.csv'.format(str(int(time)), a, r)
    if not raw_data:
        raw_data = nb.get_route_config('ttc', r)
    with open(save_path, "w+") as csv_file:
        csv_data = [['lon', 'lat', 'id', 'tag', 'title']]
        csv_w = csv.writer(csv_file, delimiter=',')
        get_time = time
        data = raw_data['route']['stop']
        c = 1
        for entry in data:
            if 'stopId' not in entry.keys():
                entry['stopId'] = ''
            csv_data.append([entry['lon'], entry['lat'], entry['stopId'], entry['tag'], entry['title']])
        csv_w.writerows(csv_data)
        csv_file.close()
        return save_path
    return ''


def log_route_route(a, r, raw_data=None, time=time.time()):
    save_path = 'data_route_{}_{}_{}.csv'.format(str(int(time)), a, r)
    if not raw_data:
        raw_data = nb.get_route_config('ttc', r)
    with open(save_path, "w+") as csv_file:
        csv_data = [['tag', 'title', 'stop']]
        csv_w = csv.writer(csv_file, delimiter=',')
        get_time = time
        data = raw_data['route']['direction']
        c = 1
        for entry in data:
            csv_data.append(
                [entry['tag'], entry['title'], entry['stop']])  # TODO: Change json of stop to *stopon raw csv
        csv_w.writerows(csv_data)
        csv_file.close()
        return save_path
    return ''

def qs_test():
    print('log_route.py')
    time = time.time()
    print('Time used for saving:', time)
    import sys

    print('done sys import')
    if len(sys.argv) < 2:
        sys.argv.append(input('Not enough arguments; enter agency tag.\nAgency Tag: '))
        route_id = input('Route ID needed.\nRoute ID: ')
    else:
        route_id = sys.argv[2]
    print('start _log_route_stop')
    log_route_stop(sys.argv[1], route_id, time)
    print('start _log_route_route')
    log_route_route(sys.argv[1], route_id, time)
    print('done')

def qs():
    print('log_route.py')
    time = time.time()
    print('Time used for saving:', time)
    import sys

    print('done sys import')
    sys.argv.append(input('Agency Tag: '))
    route_id =      input('Route ID:   ')
    print('start _log_route_stop')
    log_route_stop(sys.argv[1], route_id, time)
    print('start _log_route_route')
    log_route_route(sys.argv[1], route_id, time)
    print('done')


if __name__ == '__main__':
    qs_test()
