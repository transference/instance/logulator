import googlemaps, time, csv
from datetime import datetime
from geopy import distance
geopy_dist = distance
# GC Maps Directions API
coords_0 = '43.70721,-79.3955999'
coords_1 = '43.7077599,-79.39294'
available_apis = ['googlemaps', 'geopy'] # TODO: Workaround for #2.
def get_distance(coords_0, coords_1, api='googlemaps'):
    if api == 'googlemaps':
        coords_0 = '{lat}, {lon}'.format(lon=coords_0[0], lat=coords_0[1])
        coords_1 = '{lat}, {lon}'.format(lon=coords_1[0], lat=coords_1[1])
        # GC Maps Directions API
        # coords_0 = '43.70721,-79.3955999'
        # coords_1 = '43.7077599,-79.39294'
        gmaps = googlemaps.Client(key="AIzaSyBAFZj11dNs_6od98sAFJnSppVaA4VwJgk")
        # Request directions via public transit
        now = datetime.now()
        directions_result = gmaps.directions(coords_0, coords_1, mode="driving", departure_time=now, avoid='tolls')

        distance = 0
        legs = directions_result[0].get("legs")
        for leg in legs:
            distance = distance + leg.get("distance").get("value")
        return distance
    elif api == 'geopy':
        return geopy_dist.distance(coords_0, coords_1).km

if __name__ == '__main__':
    now_time = time.time()
    def logit(string):
        with open('log_apicomp_{}.log'.format(now_time), mode='a') as file:
            file.write(str(time.time()) + ' ' + string)
    def log_print(string):
        logit(string+'\n')
        print(string)
    # log_print(get_distance(coords_0, coords_1, 'googlemaps')) TODO: Workaround for #2.
    log_print('API Comparison')
    log_print('This will compare the results of the following apis:')
    for i in range(len(available_apis)):
        log_print('{}. {}'.format(i + 1, available_apis[i]))
    log_print('Files will be saved with time: {}'.format(now_time))
    log_print('Set Step')
    lat_step = int(input('Lat Step (step*x = 180 where type(x) == type(int()))) '))
    lon_step = int(input('Lon Step (step*x = 360 where type(x) == type(int()))) '))
    lat_range = range(44, -80, lat_step)
    lon_range = range(, lon_step)
    result = [['lat0', 'lon0', 'lat1', 'lon1', 'api', 'dist']]
    input('Press [Enter] to continue: ')
    log_print('Starting...')
    for lat0 in lat_range:
        log_print('Latitude 1: {}'.format(lat0))
        for lon0 in lon_range:
            log_print('  Longtitude 1: {}'.format(lon0))
            for lat1 in lat_range:
                log_print('    Latitude 2: {}'.format(lat1))
                for lon1 in lon_range:
                    log_print('      Longtitude 2: {}'.format(lon1))
                    for api in available_apis:
                        log_print('      Using "{}" API...'.format(api))
                        log_print('      Calling get_distance with ({}, {}) ({}, {})...'.format(lon0, lat0, lon1, lat1))
                        distance = get_distance((lat0, lon0), (lat1, lon1), api=api)
                        log_print('      Done. Distance is {}.'.format(distance))
                        log_print('      Writing data...')
                        result.append([lat0, lon0, lat1, lon1, api, distance])
                        log_print('      Done. Continuing..')
    log_print('Done. Saving data @ data_apicomp_{}.csv'.format(now_time))
    with open('data_apicomp_{}_{}_{}.csv'.format(now_time, lat_step, lon_step), mode='w') as file:
        writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerows(result)
    log_print('Done.')
    log_print('Exiting with 0 (Success).')
    exit(0)