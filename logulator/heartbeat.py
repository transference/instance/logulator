import uuid
import os
import re
import sys
import traceback
from . import *
from . import time_util
from . import log_route

# Emojis:
#    Name          Used?    Desc
# 💓 Growing Heart     Used Heartbeat Logo
# 💗 Beating Heart     Used Central Status
# 💔 Broken  Heart     Used Error
# ⛔ Error   Icon      Used Error (accompanies Broken Heart)
# ⚠ Warning Icon  Not Used Warning
# 📝 Note    Icon      Used Information (ie HBUUIDs, PUUIDs, ...)



def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)

class sys_heartbeat():
    def __init__(self, ga, gak, ba, t, u, a, rtt, rid, itv, dp,
                 ctp=os.path.join(os.getcwd(), 'commands.txt'), max_=None, max_m=None, 
                 uuid_='no_uuid', # uuid for realchart
                 sl_re='data_stop_.*.csv',  sl_re_c='data_stop_.*_{a}_{rid}.csv', # sl_re = regex formula for stoplists sl_re_c = format for saving files
                 sl_fresh_limit=31556952, # sl_fresh_limit freshness limit; 10 yr old file is probably (sorry archaeologists) trash
                 rl_re='data_route_.*.csv', rl_re_c='data_route_.*_{a}_{rid}.csv',
                 rl_fresh_limit=31556952,
                 rc_re='data_rc_.*.csv',    rc_re_c='data_rc_.*_{a}_{rid}.csv',
                 rc_fresh_limit=2592000, ):
        try:
            start_time = time_util.epoch_m()
            if u: print('💓', end='')
            print('Heartbeat')
            if t: print('Init...')
            import os
            self.wdh = [] # w(orking) d(irectory) h(istory)
            self.wdh.append(os.getcwd())
            if t: print('Done.')
            if t:
                if u: print('📝', end='')
                print('Info')
                print('Geo API:     {}'.format(ga))
                print('Geo API Key: {}'.format(gak))
                print('Bus API:     {}'.format(ba))
                print('Test Mode:   {}'.format(t))
                print('Unicode:     {}'.format(u))
                print('Agency:      {}'.format(a))
                print('Route Tag:   {}'.format(rtt))
                print('Route ID:    {}'.format(rid))
                print('Interval:    {}'.format(itv))
                print('Data Path:   {}'.format(dp))
            self.ga    = ga
            self.gak   = gak
            self.ba    = ba
            self.t     = t
            self.u     = u
            self.a     = a
            self.rtt   = rtt
            self.rid   = rid
            self.itv   = itv
            self.dp    = dp
            self.sl_re = re.compile(sl_re)
            self.sl_re_c = sl_re_c
            self.sl_fresh_limit = sl_fresh_limit
            self.rl_re = re.compile(rl_re)
            self.rl_re_c = rl_re_c
            self.rl_fresh_limit = rl_fresh_limit
            self.rc_re = re.compile(rc_re)
            self.rc_re_c = rc_re_c
            self.rc_fresh_limit = rc_fresh_limit
            self.ctp = ctp
            self.puuid = uuid.uuid4()
            self.max_ = max_
            self.max_m = max_m
            self.stop_cmd = '{} {}'.format(self.puuid, 'stop')
            print('Time (ms): {}'.format(time_util.epoch_ms()))
            print('PUUID:     {}'.format(self.puuid))
            print('^The Process UUID is printed above.')
            print('To stop this heartbeat process, write "{}" in "{}"'.format(self.stop_cmd, self.ctp))
            try:
                with open(ctp, 'r') as file:
                    if self.stop_cmd in file.read():
                        print('Command: stop')
                        print('Target:  Heartbeat Process {}'.format(self.puuid))
                        exit_(0)
            except FileNotFoundError as err:
                msg = str(err)
                print('File "{}" not found, creating file...'.format(self.ctp))
                with open(ctp, 'w') as file:
                    file.write(
                        'Transference Instance Heartbeat Command Buffer\nCreated by Heartbeat Process {}'.format(self.puuid))
                print('Done.')
            else:
                print('File "{}" found'.format(ctp))
            self.hbn = 0  # H(eart)b(eat) n(umber)
            self.c = 0
            while True:
                try:
                    if u: print('💓', end='')
                    if self.max_ != None:
                        if self.c >= self.max_:
                            do_hb = False
                            break
                        self.c += 1
                    if self.max_m != None:
                        now_time = time_util.epoch_m()
                        diff_time = now_time - start_time
                        if diff_time >= self.max_m:
                            do_hb = False
                            break
                    self.hbuuid = uuid.uuid4()
                    self.skip_cmd = '{} {}'.format(self.hbuuid, 'skip')
                    with open(self.ctp, 'r') as file:
                        if self.stop_cmd in file.read():
                            print('Command: stop')
                            print('Target:  Heartbeat Process {}'.format(self.puuid))
                            exit_(0)
                        elif self.skip_cmd in file.read():
                            print('Command: skip')
                            print('Target:  Heartbeat #{} or {}'.format(self.hbn, self.hbuuid))
                            do_hb = False
                        else:
                            do_hb = True
                    print('Heartbeat')
                    if u: print('📝', end='')
                    print('Time (ms): {}'.format(time_util.epoch_ms()))
                    print('PUUID:     {}'.format(self.puuid))
                    print('HbN:       {}'.format(self.hbn))
                    print('HBUUID:    {}'.format(self.hbuuid))
                    print('Do:        {}'.format(do_hb))
                    print('Max_:      {}'.format(self.max_))
                    print('Counter:   {}'.format(self.c))
                    print('To stop this heartbeat process, write "{}" in "{}"'.format(self.stop_cmd, ctp))
                    print('To skip this heartbeat,         write "{}" in "{}"'.format(self.skip_cmd, self.ctp))
                    if do_hb:
                        if u: print('💗', end='')
                        print('Doxtukunn')
                        if t: print('Gen dfs', end='')
                        self.dfs = []  # d(ata) f(ile)s
                        files = os.scandir(self.dp)
                        for entry in files:
                            print('.', end='')
                            if entry.is_file():
                                self.dfs.append(entry.name)
                        if t: print('\nDone.')
                        if t: print('Finding sls, rls, rcs...')
                        self.find_sls()
                        self.find_rls()
                        self.find_rcs()
                        if t: print('Done.')
                        if t: print('Checking sls...')
                        self.check_sls()
                        if t: print('Done.')
                        if t: print('Checking rls...')
                        self.check_rls()
                        if t: print('Done.')
                        if t: print('Checking rcs...')
                        self.check_rcs()
                        if t: print('Done.')
                    print('Incr hbn...')
                    self.hbn += 1
                    print('Done.')
                except Exception as err:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    cls = str(type(err)).replace('<class \'', '').replace('\'>', '') # class
                    msg = str(err) # message
                    if u: print('💔', end='')
                    print('Interrupted (inside):')
                    print('{}: {}'.format(cls, msg))
                    trace_back = traceback.extract_tb(exc_traceback)
                    stack_trace = ''
                    for i in range(len(trace_back)):
                        trace = trace_back[i]
                        n = len(trace_back) - i
                        if 2 - len(str(n)) > 0:
                            n = str(n) + (2 - len(str(n))) * ' '
                        stack_trace += '''{}:{}\n{}{}\n    {}\n'''.format(trace[0], trace[1], n, trace[2], trace[3])
                    print('Traceback:')
                    print(stack_trace)
                    print('Continuing to next heartbeat...')
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            cls = str(type(err)).replace('<class \'', '').replace('\'>', '') # class
            msg = str(err) # message
            if u: print('💔', end='')
            print('Interrupted (outside):')
            print('{}: {}'.format(cls, msg))
            trace_back = traceback.extract_tb(exc_traceback)
            stack_trace = ''
            for i in range(len(trace_back)):
                trace = trace_back[i]
                n = len(trace_back) - i
                if 2 - len(str(n)) > 0:
                    n = str(n) + (2 - len(str(n))) * ' '
                stack_trace += '''{}:{}\n{}{}\n    {}\n'''.format(trace[0], trace[1], n, trace[2], trace[3])
            print('Traceback:')
            print(stack_trace)
            exit_(422537) # Telephone Keypad

    def find_sls(self):
        self.sls = list(filter(self.sl_re.match, self.dfs))
    def check_sls(self, rd=0): # r(ecursion )d(epth)
        sls_cands = list(filter(re.compile(self.sl_re_c.format(a=self.a, rid=self.rid)).match, self.sls))
        if len(sls_cands) == 0:
            self.gen_sls()
            if rd <= 10:
                self.check_sls(rd=rd+1)
        for i in range(len(sls_cands)):
            sls_cand = sls_cands[i]
            cand_time = re.split('_', sls_cand)[2]
            now_time  = time_util.epoch_s()
            if int(cand_time) - now_time > self.sl_fresh_limit:
                self.gen_sls()
                if rd <= 10:
                    self.check_sls(rd=rd+1)
        self.sls = sls_cands
        if len(self.sls) == 0:
            self.gen_sls()
            if rd <= 10:
                self.check_sls(rd=rd+1)
        else:
            self.slp = self.sls[0]
    def gen_sls(self):
        self.wdh.append(self.dp)
        os.chdir(self.dp)
        log_route.log_route_stop(self.a, self.rid)
        self.wdh.append(self.wdh[0])
        os.chdir(self.wdh[0])

    def find_rls(self):
        self.rls = list(filter(self.rl_re.match, self.dfs))
    def check_rls(self, rd=0):
        rls_cands = list(filter(re.compile(self.rl_re_c.format(a=self.a, rid=self.rid)).match, self.rls))
        if len(rls_cands) == 0:
            self.gen_rls()
            if rd <= 10:
                self.check_rls(rd=rd+1)
        for i in range(len(rls_cands)):
            rls_cand = rls_cands[i]
            cand_time = re.split('_', rls_cand)[2]
            now_time = time_util.epoch_s()
            if int(cand_time) - now_time > self.rl_fresh_limit:
                self.gen_rls()
                if rd <= 10:
                    self.check_rls(rd=rd+1)
        self.rls = rls_cands
        if len(self.rls) == 0:
            self.gen_rls()
            if rd <= 10:
                self.check_rls(rd=rd+1)
        else:
            self.rlp = self.rls[0]
    def gen_rls(self):
        self.wdh.append(self.dp)
        os.chdir(self.dp)
        log_route.log_route_route(self.a, self.rid)
        self.wdh.append(self.wdh[0])
        os.chdir(self.wdh[0])

    def find_rcs(self):
        self.rcs = list(filter(self.rc_re.match, self.dfs))
    def check_rcs(self, rd=0):
        rcs_cands = list(filter(re.compile(self.rc_re_c.format(a=self.a, rid=self.rid)).match, self.rcs))
        if len(rcs_cands) == 0:
            self.gen_rcs()
            if rd <= 10:
                self.check_rcs(rd=rd+1)
        for i in range(len(rcs_cands)):
            rcs_cand = rcs_cands[i]
            cand_time = re.split('_', rcs_cand)[2]
            now_time  = time_util.epoch_s()
            if int(cand_time) - now_time > self.rc_fresh_limit:
                self.gen_rcs()
            if rd <= 10:
                self.check_rcs(rd=rd+1)
        self.rcs = rcs_cands
        if len(self.rcs) == 0:
            self.gen_rcs()
            if rd <= 10:
                self.check_rcs(rd=rd+1)
        else:
            self.rcp = self.rcs[0]
    def gen_rcs(self):
        self.wdh.append(self.dp)
        os.chdir(self.dp)
        get_len.qs_rc(self.slp, self.rtt, self.rid, self.a, self.ga, self.ba)
        self.wdh.append(self.wdh[0])
        os.chdir(self.wdh[0])

class sv_heartbeat():
    def __init__(self, t, u, ctp=os.path.join(os.getcwd(), 'commands.txt')):
        try:
            if u: print('💓', end='')
            print('Heartbeat')
            if t: print('Init...')
            import os
            wdh = [] # w(orking) d(irectory) h(istory)
            wdh.append(os.getcwd())
            if t: print('Done.')
            if t:
                if u: print('📝', end='')
                print('Info')
                print('Ct Path:     {}'.format(ctp))
                print('Test Mode:   {}'.format(t))
                print('Unicode:     {}'.format(u))
            self.t = t
            self.u = u
            self.ctp = ctp
            self.puuid = uuid.uuid4()
            self.stop_cmd = '{} {}'.format(self.puuid, 'stop')
            print('Time (ms): {}'.format(time_util.epoch_ms()))
            print('PUUID:     {}'.format(self.puuid))
            print('^The Process UUID is printed above.')
            print('To stop this heartbeat process, write "{}" in "{}"'.format(self.stop_cmd, self.ctp))
            try:
                with open(ctp, 'r') as file:
                    if self.stop_cmd in file.read():
                        print('Command: stop')
                        print('Target:  Heartbeat Process {}'.format(self.puuid))
                        exit_(0)
            except FileNotFoundError as err:
                msg = str(err)
                print('File "{}" not found, creating file...'.format(self.ctp))
                with open(ctp, 'w') as file:
                    file.write(
                        'Transference Instance Heartbeat Command Buffer\nCreated by Heartbeat Process {}'.format(self.puuid))
                print('Done.')
            else:
                print('File "{}" found'.format(ctp))
            self.hbn = 0  # H(eart)b(eat) n(umber)
            while True:
                if u: print('💓', end='')
                self.hbuuid = uuid.uuid4()
                self.skip_cmd = '{} {}'.format(self.hbuuid, 'skip')
                print('Heartbeat')
                if u: print('📝', end='')
                print('Time (ms): {}'.format(time_util.epoch_ms()))
                print('PUUID:  {}'.format(self.puuid))
                print('HbN:    {}'.format(self.hbn))
                print('HBUUID: {}'.format(self.hbuuid))
                print('To stop this heartbeat process, write "{}" in "{}"'.format(self.stop_cmd, ctp))
                print('To skip this heartbeat,         write "{}" in "{}"'.format(self.skip_cmd, self.ctp))
                with open(self.ctp, 'r') as file:
                    if self.stop_cmd in file.read():
                        print('Command: stop')
                        print('Target:  Heartbeat Process {}'.format(self.puuid))
                        exit_(0)
                    elif self.skip_cmd in file.read():
                        print('Command: skip')
                        print('Target:  Heartbeat #{} or {}'.format(self.hbn, self.hbuuid))
                        do_hb = False
                    else:
                        do_hb = True
                if do_hb:
                    if u: print('💗', end='')
                    print('Doxtukunn')
                    print('Bus Arrival Time (Relative) _ [] X')
                    print('WIP')
                    rtt = input('Route Tag: ')
                    stt = input('Stop  Tag: ')
                    print('Not available yet.')
                print('Incr hbn...')
                self.hbn += 1
                print('Done.')
        except KeyboardInterrupt as err:
            if u: print('💔', end='')
            print('Interrupted. (Possibly Ctrl-C)')
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            cls = str(type(err)).replace('<class \'', '').replace('\'>', '') # class
            msg = str(err) # message
            if u: print('💔', end='')
            print('Interrupted:')
            print('{}: {}'.format(cls, msg))
            trace_back = traceback.extract_tb(exc_traceback)
            stack_trace = ''
            for i in range(len(trace_back)):
                trace = trace_back[i]
                n = len(trace_back) - i
                if 2 - len(str(n)) > 0:
                    n = str(n) + (2 - len(str(n))) * ' '
                stack_trace += '''{}:{}\n{}{}\n    {}\n'''.format(trace[0], trace[1], n, trace[2], trace[3])
            print('Traceback:')
            print(stack_trace)
    def server_flask(self):
        pass

def rc_heartbeat(slp, rtt, rid, a, ga='googlemaps', ba='nextbus', uuid_=None, interval=1000, test=False, ctp=os.path.join(os.getcwd(), 'commands.txt'), unicode=True):
    try:
        puuid = uuid.uuid4()
        stop_cmd = '{} {}'.format(puuid, 'stop')
        if test:
            called = get_len.qs_rc_test
        else:
            called = get_len.qs_rc
            print('Time (ms): {}'.format(time_util.epoch_ms()))
        print('PUUID:     {}'.format(puuid))
        print('^The Process UUID is printed above.')
        print('To stop this heartbeat process, write "{}" in "{}"'.format(stop_cmd, ctp))
        try:
            with open(ctp, 'r') as file:
                if stop_cmd in file.read():
                    print('Command: stop')
                    print('Target:  Heartbeat Process {}'.format(puuid))
                    exit_(0)
        except FileNotFoundError as err:
            msg = str(err)
            print('File "{}" not found, creating file...'.format(ctp))
            with open(ctp, 'w') as file:
                file.write('Transference Instance Heartbeat Command Buffer\nCreated by Heartbeat Process {}'.format(puuid))
            print('Done.')
        else:
            print('File "{}" found'.format(ctp))
        hbn = 0 # H(eart)b(eat) n(umber)
        while True:
            try:
                if unicode: print('💓', end='')
                hbuuid = uuid.uuid4()
                skip_cmd = '{} {}'.format(hbuuid, 'skip')
                print('Heartbeat')
                if unicode:
                    print('📝Time (ms): {}'.format(time_util.epoch_ms()))
                    print(' PUUID:  {}'.format(puuid))
                    print(' HbN:    {}'.format(hbn))
                    print(' HBUUID: {}'.format(hbuuid))
                    print(' To stop this heartbeat process, write "{}" in "{}"'.format(stop_cmd, ctp))
                    print(' To skip this heartbeat,         write "{}" in "{}"'.format(skip_cmd, ctp))
                else:
                    print('Time (ms): {}'.format(time_util.epoch_ms()))
                    print('PUUID:  {}'.format(puuid))
                    print('HbN:    {}'.format(hbn))
                    print('HBUUID: {}'.format(hbuuid))
                    print('To stop this heartbeat process, write "{}" in "{}"'.format(stop_cmd, ctp))
                    print('To skip this heartbeat,         write "{}" in "{}"'.format(skip_cmd, ctp))
                with open(ctp, 'r') as file:
                    if stop_cmd in file.read():
                        print('Command: stop')
                        print('Target:  Heartbeat Process {}'.format(puuid))
                        exit_(0)
                    elif skip_cmd in file.read():
                        print('Command: skip')
                        print('Target:  Heartbeat #{} or {}'.format(hbn, self.hbuuid))
                        do_hb = False
                    else:
                        do_hb = True
                if do_hb:
                    if unicode: print('💗Calling qs_rc(_test)...')
                    else: print('Calling qs_rc(_test)...')
                    called(slp, rtt, rid, a, ga, ba, uuid_)
                    print('Done.')
                print('Incr hbn...')
                hbn += 1
                print('Done.')
            except Exception as err:
                if unicode: print('💔⛔Error occured. ({}: {})'.format(str(type(err)).replace('<class \'', '').replace('\'>', ''), str(err)))
                else:       print('Error occured. ({}: {})'.format(str(type(err)).replace('<class \'', '').replace('\'>', ''), str(err)))
        print('Heartbeat stopped.')
        exit_(0)
    except KeyboardInterrupt:
        if unicode: print('💔Interrupted. (Possibly Ctrl-C)')
        else:       print('Interrupted. (Possibly Ctrl-C)')

def qs_hb_sys(ga, gak, ba, t, u, a, rtt, rid, itv, dp):
    inst = sys_heartbeat(ga, gak, ba, t, u, a, rtt, rid, itv, dp)

if __name__ == '__main__':
    print('Logulator Heartbeat')
    print('USE ONLY FOR TESTING OR DEBUGGING')
    print('Calling rc_heartbeat')

    rc_heartbeat()
