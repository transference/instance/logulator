import datetime, time

def day_us():
    dt = datetime.datetime.now()
    return dt.hour*60*60*1000+dt.minute*60*1000+dt.second*1000+dt.microsecond

def day_s():
    dt = datetime.datetime.now()
    return dt.hour*60*60+dt.minute*60+dt.second

def week_ms():
    dt = datetime.datetime.now()
    return dt.hour*60*60*1000+dt.minute*60*1000+dt.second*1000+dt.microsecond + datetime.datetime.today().weekday()*24*60*60*1000

def week_s():
    return int(week_ms())

def day_day():
    dt = datetime.datetime.now()
    return int(dt.isocalendar()[1]*7 + dt.isocalendar()[2])

def epoch_ms():
    return int(time.time()*1000)

def epoch_s():
    return int(time.time())

def epoch_m():
    return int(time.time()/60)

def epoch_h():
    return int(time.time()/60/60)
