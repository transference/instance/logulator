import nextbus



def qs(slp, rtt, rid, a, ga='googlemaps', ba='nextbus', uuid='no_uuid'):
    print('Get Real Chart WIP')
    print('Stoplist Path:  {}'.format(slp))
    print('Route Tag:      {}'.format(rtt))
    if slp == '':
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rtt == '':
        rtt = '34_1_34Akes'
    with open(slp, 'r') as csv_file:
        sl = csv.reader(slp)[1:]
    import requests, nextbus
    nb = nextbus.qs()
    try:
        buses = nb.get_route_loc(a, rid)['vehicle']
    except requests.exceptions.ConnectionError as error:
        print('Bus Loc API is unreacheable. (requests.exceptions.ConnectionError: {})'.format(str(error)))
        return
    data = _get_real_chart(sl, rtt, buses, ga, a, api)
    with open('data_rc_{}_{}_{}_{}.csv'.format(time_util.epoch_ms(), a, rtt, uuid), 'w') as file:
        csv_file = csv.writer(file)
        csv_file.writerows(data)

def qs_test(slp, rtt, rid, a, ga='googlemaps', ba='nextbus', uuid='no_uuid'):
    print('Get Real Chart Test WIP')
    if slp == '':
        warnings.warn('slp not specified or ""; replaced with "data_stop_1568411857_ttc_34.csv".')
        slp = 'data_stop_1568411857_ttc_34.csv'
    if rtt == '':
        warnings.warn('rtt not specified or ""; replaced with "34_1_34Akes"')
        rtt = '34_1_34Akes'
    print('Stoplist Path:  {}'.format(slp))
    print('Route Tag:      {}'.format(rtt))
    with open(slp, 'r') as csv_file:
        sl = list(csv.reader(csv_file))[1:]
    import requests, nextbus
    nb = nextbus.qs()
    try:
        buses = nb.get_route_loc(a, rid)['vehicle']
    except requests.exceptions.ConnectionError as error:
        print('Bus Loc API is unreacheable. (requests.exceptions.ConnectionError: {})'.format(str(error)))
        print('NextBus Webservices is unreachable. (requests.exceptions.ConnectionError)')
        print('The following conditions need to be met:')
        print('1. Connection to webservices.nextbus.com via HTTP(S)')
        print('Which usually need:')
        print('1. Connection to the Internet via HTTP(S)')
        print('2. No firewalls blocking access to webservices.nextbus.com or the Internet')
        print('3. No Wi-Fi/Local Network signin pages (eg Guest Wi-Fi signin pages)')
        print('4. TCP to be enabled on the local network, if it exists.')
        return
    data = _get_real_chart(sl, rtt, buses, ga, a, api)
    with open('data_rc_test_{}_{}_{}_{}.csv'.format(time_util.epoch_ms(), a, rtt, uuid), 'w') as file:
        csv_file = csv.writer(file)
        csv_file.writerows(data)
    print('Done.')