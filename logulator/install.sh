#!/bin/sh
echo "V2.1.0"
echo "Transference Instance Logulator [Re]Installer Script "
echo "LEGACY NOT RECOMMENDED"
echo "Choose the Installation Path"
echo "Default: /usr/local/"
read -p "Installation Path: " INSTALL_PATH
echo "Choose the Link Path"
echo "Default: /usr/bin"
read -p "Link Path:         "    LINK_PATH
echo "Getting current user..."
export C_USER=$(/usr/bin/id -run)
echo "Done. C_USER is:"
echo $C_USER
read -p "Install prerequisites? (Y/n): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo
    echo "Confirmed YES"
    echo "SUDO Installing git, python3, python3-pip (if not already installed)..."
    sudo apt install git python3 python3-pip -y
    echo "Done."
    echo "Getting requirements.txt..."
    curl https://gitlab.com/transference/instance/logulator/raw/db90dde90be7973d0ed435c43e3892799dafb406/logulator/requirements.txt > logulator_prerequisites.txt
    echo "Done."
    echo "SUDO Installing prerequisites..."
    sudo python3 -m pip install requests googlemaps geopy
    echo "Done."
    echo "Removing requirements.txt"
    rm logulator_prerequisites.txt
    echo "Done."
    echo "SUDO Removing old installation (if exists)..."
    cd $INSTALL_PATH
    sudo rm -r logulator
    cd $LINK_PATH
    sudo rm logulator
    echo "Done."
    echo "Changing WD..."
    cd $INSTALL_PATH
    echo "Done."
    echo "Current User:     " $C_USER
    echo "Installation Path:" $INSTALL_PATH
    echo "Link         Path:"$LINK_PATH
    read -p "Install? (Y/n):    " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo
        echo "Confirmed YES"
        echo "SUDO Cloning..."
        sudo git clone https://gitlab.com/transference/instance/logulator.git
        echo "Done."
        echo "SUDO Adding symlink..."
        sudo ln -s $INSTALL_PATH"/logulator/__init__.py" $LINK_PATH"/logulator"
        echo "SUDO Chown..."
        sudo chown $C_USER:$C_USER $INSTALL_PATH"/logulator/__init__.py"
        sudo chown $C_USER:$C_USER $LINK_PATH"/logulator"
        echo "Done."
        echo "Installed."
        echo "Current User:     " $C_USER
        echo "Installation Path:" $INSTALL_PATH
        echo "Link         Path:"$LINK_PATH
    else
        echo
        echo "Confirmed NO"
        echo "Aborted."
    fi
else
    echo
    echo "Confirmed NO"
    echo "Aborted."
