def format_route_csv(path):
    old_file = open(path, 'r')
    old_cont = old_file.read()
    old_file.close()
    del old_file
    file = open(path, 'w')
    file.write(old_cont.replace("{'tag': ", '').replace('}', ''))
    file.close()


if __name__ == '__main__':
    print('format_route_csv.py')
    path = input('Route CSV (data_route_1568414604_ttc_34.csv): ')
    if path == '':
        path = 'data_route_1568414604_ttc_34.csv'
    format_route_csv(path)
    print('Done. Overwritten as {}.'.format(path))
