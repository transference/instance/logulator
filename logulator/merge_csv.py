import time


def _csv_merge(destination_path, *source_paths):
    '''
    Merges all csv files on source_paths to destination_path.
    :param destination_path: Path of a single csv file, doesn't need to exist
    :param source_paths: Paths of csv files to be merged into, needs to exist
    :return: None
    '''
    with open(destination_path, "a") as dest_file:
        with open(source_paths[0]) as src_file:
            for src_line in src_file.read():
                dest_file.write(src_line)
        source_paths.pop(0)
        for i in range(len(source_paths)):
            with open(source_paths[i]) as src_file:
                src_file.next()
                for src_line in src_file:
                    dest_file.write(src_line)
    return None


def csv_merge(*source_paths): # TODO: Delete duplicate entries of stop data.
    '''
    Merges route-specific source files into one agency-specific one
    :param source_paths: Paths of source csvs
    :return: None
    '''
    return _csv_merge('data_stoplist_{}_{}'.format(str(int(time.time())), source_paths[0].split('_')[3], *source_paths))
