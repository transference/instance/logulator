import csv
import time
from . import nextbus
qs = nextbus.qs

def exit_(code=0):
    print('Exiting with code {}...'.format(code))
    exit(code)


nb = qs()


def log_csv(a):
    with open("data_" + str(int(time.time())) + '.csv', "w+") as csv_file:
        print('done open file')
        csv_data = [['agency', 'route', 'id', 'time', 'lon', 'lat', 'direction', 'hdg']]
        csv_w = csv.writer(csv_file, delimiter=',')
        print('done csv init')
        data = nb.get_route_loc_nr(a, '0')['vehicle']
        get_time = time.time()
        print('done nextbus.qs.get_route_loc_nr')
        c = 1
        print('parsing data to csv_________________________________________________________________________________')
        for entry in data:
            csv_data.append(
                [a, entry['routeTag'], entry['id'], str(int(get_time) - int(entry['secsSinceReport'])), entry['lon'],
                 entry['lat'], entry['dirTag'], entry['heading']])
            if c % int(len(data) / 100) == 0:
                print('.', end='')
            c += 1
        print('\ndone loop')
        csv_w.writerows(csv_data)
        print('done writerows')
        csv_file.close()

def qs():
    try:
        import sys
    except ImportError as error:
        msg = str(error)
        print('Python Standard Library "sys" possibly not found. (ImportError: {})'.format(msg))
        exit_(2)
    else:
        print('Library "sys" imported successfully.')
    a = input('Agency (ttc): ')
    if a == '':
        a = 'ttc'
    print('Starting...')
    log_csv(a)
    print('Done.')

def qs_test():
    print('lo_bus.py')
    try:
        import sys
    except ImportError as error:
        msg = str(error)
        print('Python Standard Library "sys" possibly not found. (ImportError: {})'.format(msg))
        exit_(2)
    else:
        print('Library "sys" imported successfully.')
    if len(sys.argv) < 2:
        error_not_enough_args = 'Usage python3 log2.py [agency_tag]'
        raise RuntimeError(error_not_enough_args)
    a = input('Agency (ttc): ')
    if a == '':
        a = 'ttc'
    print('Starting (log_csv)...')
    log_csv(a)
    print('Done.')


if __name__ == '__main__':
    qs_test()
