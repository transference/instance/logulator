import json
import requests
import time


class nextbus():
    def __init__(self, base_url='http://webservices.nextbus.com/service/publicJSONFeed', type='json'):
        self.type = type.lower()
        self.base_url = base_url
        self.agency_list = None

    def update_agency_list(self):  # This updates the list of agencies
        self.agency_list = json.loads(requests.get(self.base_url + '?command=agencyList').text)['agency']

    def get_route_list(self, agency_tag):  # This is the list of routes
        return json.loads(requests.get(self.base_url + '?command=routeList&a=' + agency_tag).text)

    def get_route_config(self, agency_tag,
                         route_tag):  # This is route names, colours, tags, agencies, ..., and route path on the branch(es)
        return json.loads(requests.get(self.base_url + '?command=routeConfig&a=' + agency_tag + '&r=' + route_tag).text)

    def get_route_loc(self, agency_tag, route_tag, time=0):  # This gets the location of a vehicle from a route
        # print(self.base_url + '?command=vehicleLocations&a=' + agency_tag + '&r=' + route_tag + '&t=' + str(time))
        return json.loads(requests.get(
            self.base_url + '?command=vehicleLocations&a=' + agency_tag + '&r=' + route_tag + '&t=' + str(time)).text)

    def get_route_loc_nr(self, agency_tag, time=0):  # This gets the location of a vehicle
        return json.loads(requests.get(self.base_url + '?command=vehicleLocations&a=' + agency_tag + '&t=' + time).text)

    def _get_route_schedule(self, agency_tag, route_tag):
        return json.loads(requests.get(self.base_url + '?command=schedule&a=' + agency_tag + '&r=' + route_tag).text)

    def get_route_schedule(self, agency_tag, route_tag=None, recursion_count=0):
        '''
        Gets route schedule
        :param agency_tag: Agency Tag
        :type agency_tag: str
        :param route_tag: Route Tag. If None, iterates through all routes
        :type route_tag: str
        :return:
        '''
        if recursion_count > 10:
            return
        if not route_tag:
            routes = self.get_route_list(agency_tag)['route']
            returner = []
            for i in range(len(routes)):
                result = self._get_route_schedule(agency_tag, routes[i])
                if 'Error' in result.keys():
                    print('NextBus Error:', end=' ')
                    if 'You have requested too much data in the last ' in result['Error']['content']:
                        t, b = result['Error']['content'].replace('You have requested too much data in the last ',
                                                                  '').replace('seconds. Limit is ', '').replace(
                            ' bytes.', '').split(' ')
                        print('Overlimit {} B/{}s waiting {} s...'.format(b, t, t), end=' ')
                        time.sleep(int(t))
                        print('Recursing...')
                        returner = self.get_route_schedule(agency_tag, route_tag, recursion_count + 1)
                    else:
                        print('{}'.format(result['Error']['content']), end=' ')
                returner.append(result['route'])
            return returner
        else:
            result = self._get_route_schedule(agency_tag, route_tag)
            if 'Error' in result.keys():
                print('NextBus Error:', end=' ')
                if 'You have requested too much data in the last ' in result['Error']['content']:
                    t, b = result['Error']['content'].replace('You have requested too much data in the last ', '').replace('seconds. Limit is ', '').replace(' bytes.', '').split(' ')
                    print('Overlimit {} B/{}s waiting {} s'.format(b, t, t), end=' ')
                    time.sleep(int(t))
                    print(' Recursing...')
                    returner = self.get_route_schedule(agency_tag, route_tag, recursion_count + 1)
                else:
                    print('{}'.format(result['Error']['content']), end=' ')
            else:
                return result['route']



def qs():  # QuickStart
    return nextbus()

if __name__ == '__main__':
    nb = qs()
    print(nb.get_route_loc(input(), input()))
