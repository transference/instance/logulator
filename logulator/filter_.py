import csv


def filter_(file_list_path, target_sid, save_path, test=False):
    if test: print('Reading File List...')
    if test: print('Open...')
    file_list_file = open(file_list_path, 'r')
    if test: print('Read...')
    file_list_cont = file_list_file.read()
    file_list_cont = file_list_cont.split('\n') # TODO: Make code cleaner
    if test: Print('Close...')
    file_list_file.close()
    if test: Print('Done.')

    result = ''

    if test: Print('Loop...')
    for i in range(len(file_list_cont)):
        file_path = file_list_cont[i]
        print('{}'.format(file_path), end=' ')
        try:
            file_file = open(file_path, 'r')
        except FileNotFoundError:
            print('Not Found', end=' ')
            print()
            continue
        except IsADirectoryError:
            print('Dir', end=' ')
        else:
            print('OK', end=' ')
        file_cont = file_file.read()
        file_file.close()
        file_lines = file_cont.split('\n')
        print(len(file_lines), end='')
        for j in range(len(file_lines)):
            line = file_lines[j]
            if j == 0:
                print('X', end='')
            if target_sid in line:
                print('Y', end='')
                result += line + '\n'
            else:
                print('N', end='')
        print()

    if test: 'Done.')
    if test: 'Saving result...')
    save_file = open(save_path, 'w')
    save_file.write(result)
    save_file.close()
    if test: 'Done.')

def qs(file_list_path=input('File List Path: '), target_sid=input('Target SID:     '), save_path=input('Save Path:      '))
    print('Transference Instance Logulator Filter')
    filter_(file_list_path, target_sid, save_path, test=False)

def qs_test(file_list_path=input('File List Path: '), target_sid=input('Target SID:     '), save_path=input('Save Path:      '))
    print('Transference Instance Logulator Filter Test')
    filter_(file_list_path, target_sid, save_path, test-True)
